package fr.umlv.users;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.umlv.json.UserAuth;
import fr.umlv.json.EmailJSON;
import fr.umlv.rmi.Car;
import fr.umlv.rmi.Mark;
import fr.umlv.rmi.Notification;
import fr.umlv.rmi.Rental;
import fr.umlv.rmi.Renter;
import fr.umlv.rmi.User;

@Path("/users")
public class UserServices {
	

	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getClient(UserAuth clientAuth) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		boolean success = false;
		Renter renter;
			try {
				renter = (Renter) Naming.lookup("Renter");
				User user = renter.authentication(clientAuth.getUsername(),clientAuth.getPassword());
				if (user != null) {
					success = true;
					responseBuilder.add("firstName", user.getFirstName());
					responseBuilder.add("lastName", user.getName());
					responseBuilder.add("type",user.getUserType().name());
				}
				
				responseBuilder.add("success",success);
				
				
			} catch (MalformedURLException | RemoteException | NotBoundException e) {
				e.printStackTrace();
			}
		
		JsonObject responseJsonObject = responseBuilder.build();
		return Response.ok(responseJsonObject).build();
		
	}
	
	@GET
	@Path("/getNotifs")
	@Produces(MediaType.APPLICATION_JSON)
	public String getNotifications(@QueryParam("email")String email) {
		JsonArrayBuilder responseBuilder = Json.createArrayBuilder();
		List<Notification> listNotif;
		Renter renter;
		
		try {
			renter = (Renter) Naming.lookup("Renter");
			User user = renter.findEmployeeByEmail(email);
			if (user != null) {
				listNotif = user.getNotifications();
				if (listNotif != null) {
					for (Notification notif : listNotif) {
						JsonObjectBuilder notifBuilder = Json.createObjectBuilder();
						notifBuilder.add("type",notif.getType());
						notifBuilder.add("message",notif.getMessage());
						
						responseBuilder.add(notifBuilder);
					}
				}
			}
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	
		
		JsonArray responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	
	
	@POST
	@Path("/removeNotifs")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeNotifications(EmailJSON email) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		boolean success = false;
		Renter renter;
		
		try {
			renter = (Renter) Naming.lookup("Renter");
			User user = renter.findEmployeeByEmail(email.getEmail());
			if (user != null) {
				user.removeNotifications();
				success = true;
			}
			responseBuilder.add("success",success);
						
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	
		JsonObject responseJsonObject = responseBuilder.build();
		return Response.ok(responseJsonObject).build();
	}
	
	@GET
	@Path("/getRental")
	@Produces(MediaType.APPLICATION_JSON)
	public String getRental(@QueryParam("email")String email) {
		System.out.println("email = " + email);
		Renter renter;
		Rental rental;
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		
		try {
			renter = (Renter) Naming.lookup("Renter");
			User user = renter.findEmployeeByEmail(email);
			if (user != null) {
				rental = user.getCurrentRental();
				if (rental != null) {
					JsonObjectBuilder rentalJson = Json.createObjectBuilder();
					rentalJson.add("car", getCarJson(rental.getCar()));
					rentalJson.add("start", rental.getStartDate().toString());
					rentalJson.add("end", rental.getEndDate().toString());
					
					responseBuilder.add("rental",rentalJson);
				}
			}
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	
	
	/*
	 * TODO If necessary
	private JsonObjectBuilder getUserJson(User user) {
		JsonObjectBuilder userBuilder = Json.createObjectBuilder();
		try {
			user.getEmail();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return userBuilder;
	
	}
	
	*/
	
	
	
	private JsonObjectBuilder getCarJson(Car car) {
		JsonObjectBuilder carBuilder = Json.createObjectBuilder();
		try {
			JsonArray JsonMarkArray = getJsonMarkArray(car.getMarks());
			carBuilder.add("marks", JsonMarkArray);
			carBuilder.add("id", car.getId());
			carBuilder.add("model",car.getModel());
			carBuilder.add("description",car.getDescription());
			carBuilder.add("color",car.getColor().toString());
			carBuilder.add("engine",car.getEngineType().toString());
			carBuilder.add("power",car.getEnginePower());
			carBuilder.add("price",car.getPriceRent());
			carBuilder.add("image", car.getImage());
			carBuilder.add("pendingUsers", car.getPendingUserQueue().size());
			carBuilder.add("isRent", car.getCurrentUser() != null ? true : false);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return carBuilder;
	
	}
	
	private JsonArray getJsonMarkArray(List<Mark> carMarks) throws RemoteException {
		JsonArrayBuilder carMarksBuilder = Json.createArrayBuilder();
		for(Mark mark : carMarks) {
			JsonObjectBuilder markJson = Json.createObjectBuilder();
			markJson.add("carMark", mark.getCarMark());
			markJson.add("stateMark", mark.getStateMark());
			markJson.add("opinion", mark.getOpinion());
			carMarksBuilder.add(markJson.build());
		}
		return carMarksBuilder.build();
	}

	
}
