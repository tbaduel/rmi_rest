package fr.umlv.cars;


import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.umlv.json.CarRent;
import fr.umlv.json.SetMarkJSON;
import fr.umlv.rmi.Car;
import fr.umlv.rmi.Mark;
import fr.umlv.rmi.Renter;

@Path("/cars")
public class CarServices{
	
	
	@Path("/getcars")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllCars() {
		JsonObjectBuilder carsBuilder = Json.createObjectBuilder();
		try {
			Renter renter = (Renter) Naming.lookup("Renter");
			List<Car> listcar = renter.getCarList();		
			for (Car car : listcar) {
				carsBuilder.add(Long.toString(car.getId()),getCarJson(car));
			}
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("getAllCars");
			e.printStackTrace();
	
		}
		JsonObject carsJsonObject = carsBuilder.build();
		return carsJsonObject.toString();
	}
	
	
	@GET
	@Path("/car/{carid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCarInfo(@PathParam("carid")long carId) {
		JsonObjectBuilder carsBuilder = Json.createObjectBuilder();
		Renter renter;
		try {
			renter = (Renter) Naming.lookup("Renter");
			List<Car> listcar = renter.getCarList();
			
			Optional<Car> Optcar = getCarFromList(listcar,carId);
			if (Optcar.isPresent()) {
				Car car = Optcar.get();
				carsBuilder.add(Long.toString(car.getId()),getCarJson(car));
			}	
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("getCarInfo");
			e.printStackTrace();
		}
	
		JsonObject carsJsonObject = carsBuilder.build();
		return carsJsonObject.toString();
	}
	
	
	@POST
	@Path("/rent")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String rentACar(CarRent carRent) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		Renter renter;
		try {
			renter = (Renter) Naming.lookup("Renter");
			List<Car> carlist = renter.getCarList();
			Optional<Car> optCar = getCarFromList(carlist, carRent.getCarId() );
			if(optCar.isPresent()) {
				Car car = optCar.get();
				Boolean isRent = renter.rentACar(car.getId(), carRent.getEmpMail());
				responseBuilder.add("isRent", isRent);
			}
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("rentACar");
			e.printStackTrace();
		}
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	
	
	@POST
	@Path("/release")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String releaseCar(CarRent carRent) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		Renter renter;
		try {
			renter = (Renter) Naming.lookup("Renter");
			Boolean isReleased = renter.releaseACar(carRent.getCarId());
			responseBuilder.add("isRent", isReleased);
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("releaseCar");
			e.printStackTrace();
		}
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	
	
	@POST
	@Path("/addMark")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addMarkToCar(SetMarkJSON markCar) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		Renter renter;
		Car car;
		boolean success = false;
		try {
			renter = (Renter) Naming.lookup("Renter");
			Optional<Car> optcar;
			optcar = getCarFromList(renter.getCarList(),markCar.getCarId());
			if (optcar.isPresent()) {
				car = optcar.get();
				car.addMark(markCar.getCarMark(), markCar.getStateMark(), markCar.getOpinion());
				success = true;
			}
			
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		responseBuilder.add("success", success);
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}

	
		
	private Optional<Car> getCarFromList(List<Car> listcar, long carId) {
		for (Car car : listcar) {
			try {
				if (car.getId() == carId) {
					return Optional.of(car);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return Optional.empty();
			
	}
	
	
	private JsonObjectBuilder getCarJson(Car car) {
		JsonObjectBuilder carBuilder = Json.createObjectBuilder();
		try {
			JsonArray JsonMarkArray = getJsonMarkArray(car.getMarks());
			carBuilder.add("marks", JsonMarkArray);
			carBuilder.add("id", car.getId());
			carBuilder.add("model",car.getModel());
			carBuilder.add("description",car.getDescription());
			carBuilder.add("color",car.getColor().toString());
			carBuilder.add("engine",car.getEngineType().toString());
			carBuilder.add("power",car.getEnginePower());
			carBuilder.add("price",car.getPriceRent());
			carBuilder.add("image", car.getImage());
			carBuilder.add("pendingUsers", car.getPendingUserQueue().size());
			carBuilder.add("currentRenter", car.getCurrentUser() != null ? car.getCurrentUser().getEmail() : "");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return carBuilder;
	
	}
	
	private JsonArray getJsonMarkArray(List<Mark> carMarks) throws RemoteException {
		JsonArrayBuilder carMarksBuilder = Json.createArrayBuilder();
		for(Mark mark : carMarks) {
			JsonObjectBuilder markJson = Json.createObjectBuilder();
			markJson.add("carMark", mark.getCarMark());
			markJson.add("stateMark", mark.getStateMark());
			markJson.add("opinion", mark.getOpinion());
			carMarksBuilder.add(markJson.build());
		}
		return carMarksBuilder.build();
	}

}