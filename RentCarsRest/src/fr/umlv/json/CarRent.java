package fr.umlv.json;

public class CarRent {

	
	private long carId;
	private String empMail;
	
	
	public CarRent() {
		
	}
	
	public long getCarId() {
		return carId;
	}
	public void setCarId(long carId) {
		this.carId = carId;
	}
	public String getEmpMail() {
		return empMail;
	}
	public void setEmpMail(String empMail) {
		this.empMail = empMail;
	}
	
	
}
