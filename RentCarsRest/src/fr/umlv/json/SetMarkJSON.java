package fr.umlv.json;

import java.io.Serializable;

public class SetMarkJSON implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long carId;
	private int carMark;
	private int stateMark;
	private String opinion;
	public long getCarId() {
		return carId;
	}
	public void setCarId(long carId) {
		this.carId = carId;
	}
	public int getCarMark() {
		return carMark;
	}
	public void setCarMark(int carMark) {
		this.carMark = carMark;
	}
	public int getStateMark() {
		return stateMark;
	}
	public void setStateMark(int stateMark) {
		this.stateMark = stateMark;
	}
	public String getOpinion() {
		return opinion;
	}
	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}
	
	
	
	
	
}
