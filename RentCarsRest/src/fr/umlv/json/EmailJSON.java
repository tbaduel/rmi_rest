package fr.umlv.json;

import java.io.Serializable;

public class EmailJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
