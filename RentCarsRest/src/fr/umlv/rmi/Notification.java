package fr.umlv.rmi;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Notification extends Remote{
	public String getType() throws RemoteException;
	public String getMessage() throws RemoteException;
}
