package fr.umlv.serverInit;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.umlv.rmi.User;
import fr.umlv.rmi.UserImpl;
import fr.umlv.rmi.UserType;


/**
 * Servlet implementation class ServerInit
 */
@WebServlet("/ServerInit")
public class ServerInit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * Default constructor. 
     */
    public ServerInit() {
        
    	
		try {
			// On recupere renter
			LocateRegistry.createRegistry(1099);
			System.setProperty("java.security.policy","file:./security.policy");			
			User fabien = new UserImpl("Ribeiro", "Fabien", "azerty", "fribeiro@rest.com", null, null, UserType.EMPLOYEE);
            User ichem = new UserImpl("Shafie", "Ichem", "azerty2", "ishafie@rest.com", null, null, UserType.EMPLOYEE);
            User alan = new UserImpl("Khelifa", "Alan", "qwerty", "akhelifa@rest.com", null, null, UserType.EMPLOYEE);
            User thomas = new UserImpl("Baduel", "Thomas", "qwerty2", "tbaquel@rest.com", null, null, UserType.EMPLOYEE);
            User buyer = new UserImpl("Baduel", "Thomas", "qwerty2", "tbaduel@buy.com", null, null, UserType.CUSTOMER);
            Naming.rebind("buyer", buyer);
            Naming.rebind("fabien", fabien);
            Naming.rebind("ichem", ichem);
            Naming.rebind("alan", alan);
            Naming.rebind("thomas", thomas);
		} catch (MalformedURLException | RemoteException e) {
			e.printStackTrace();

		}
    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
