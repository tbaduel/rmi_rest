import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { BehaviorSubject } from 'rxjs';

const STORAGE_KEY = 'local_todolist';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  public storeObs: BehaviorSubject<any>;
  public currentStorage = [];

  constructor(@Inject(SESSION_STORAGE) private storage: StorageService) {
    this.storeObs = new BehaviorSubject(this.currentStorage) as BehaviorSubject<any>;
  }

  public set(key: string, value: string): void {
    this.currentStorage[key] = value;
    this.storeObs.next(this.currentStorage);
    this.storage.set(key, value);
  }

  public get(key: string) {
    return (this.storage.get(key));
  }

  public clear() {
    console.log('cleared storage');
    this.currentStorage = [];
    this.storage.remove('firstName');
    this.storage.remove('lastName');
    this.storage.remove('email');
    this.storage.remove('hasCar');
    this.storeObs.next(this.currentStorage);
  }
}
