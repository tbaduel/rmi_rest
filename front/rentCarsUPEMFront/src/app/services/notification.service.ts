import { Injectable } from '@angular/core';
import { HttpService } from './http-service.service';
import { StoreService } from './store.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public notifications : Object;
  public notifObs: BehaviorSubject<Object>;
  constructor(private http: HttpService, private store: StoreService) {
    this.notifObs = new BehaviorSubject(this.notifications) as BehaviorSubject<Object>;
    setInterval(()=> {
      if (this.store.get('email')) {
        this.http.get("/users/getNotifs", {email: this.store.get('email')}).then((resp) => {
          console.log('looking for notifications:');
          console.log(resp);
          this.notifications = resp;
          this.notifObs.next(this.notifications);
        });
      }
    },60000); 
    
  }
}
