import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  constructor(public http: HttpClient, private store: StoreService) { }

  public get(url, params) {
      if (url.startsWith("/cars/")) {
        if (this.store.get('type') === "CUSTOMER") {
          url = url.replace("/cars/", "/buycar/");
        }
      }
      return this.http.get(url, {params: params}).toPromise();
  }

  public post(url, body) {
    if (url.startsWith("/cars/")) {
      if (this.store.get('type') === "CUSTOMER") {
        url = url.replace("/cars/", "/buycar/");
      }
    }
    return this.http.post(url, body).toPromise();
  }
}
