import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public store: StoreService, public router: Router) {}
  canActivate(): boolean {
    if (!this.store.get('email')) {
      this.router.navigate(['auth']);
      return false;
    }
    return true;
  }
}
