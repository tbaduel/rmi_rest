import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/http-service.service';
import {ActivatedRoute} from '@angular/router';

import {StoreService} from 'src/app/services/store.service';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})


export class CarComponent implements OnInit {
  public car = {
    color: '',
    description: '',
    engine: '',
    currentRenter: false,
    id: 0,
    image: 'placeholder.png',
    marks: [],
    model: '',
    pendingUsers: 0,
    power: 0,
    price: 0,
  };
  public currentMoney = 'EUR';
  public type: string;
  public canRent = true;
  public isRented = false;
  public currencyConverter = [];
  public currencyConverterSet: any;

  public basePrice = 0;
  public baseCurrency = 'EUR';
  public price: number = 0;

  constructor(private modal: NgxSmartModalService,
              private httpService: HttpService,
              private route: ActivatedRoute,
              private store: StoreService) {
    const id = this.route.snapshot.paramMap.get('id');
      this.httpService
        .get(`/cars/car/${id}`, []).then((data) => {
          console.log(data);
          this.car = data[id];
          this.type = this.store.get('type');
          this.basePrice = this.car['price'];
          this.price = this.car['price'];
          if (this.type == "CUSTOMER") {
            this.httpService.get('https://api.exchangeratesapi.io/latest', {}).then((resp) => {
              console.log(resp);
              this.currencyConverterSet = resp['rates'];
              this.currencyConverterSet['EUR'] = 1;
              for (let val in resp['rates']) {
                this.currencyConverter.push({id: val, rate: resp['rates'][val]});
              }
              console.log(this.currencyConverter);
            });
          }
          // Should also check for existing location in session
          console.log(this.store.get('hasCar'));
          if (this.store.get('hasCar'))
            this.canRent = this.store.get('hasCar') === "n";
        }
      );

  }

  ngOnInit() {
    
  }



  public rentThisCar() {
    this.httpService.post('/cars/rent', {carId: this.car.id, empMail: this.store.get('email')})
        .then((data) => {
          if (data && data['isRent'] === true) {
            this.isRented = true;
            this.store.set('hasCar', "y");
          }
          this.modal.open('rentingModal');
        });
  }

  public buyThisCar() {
    console.log('buying..');
    this.httpService.post('/cars/buy', {carId: this.car.id, empMail: this.store.get('email'), currency: this.currentMoney})
        .then((data) => {
          console.log(data);
        });
  }

  public selectChange(event) {
    console.log('changed to: ' + event);
    this.currentMoney = event;
    this.price = this.basePrice * this.currencyConverterSet[event];
  }
}

