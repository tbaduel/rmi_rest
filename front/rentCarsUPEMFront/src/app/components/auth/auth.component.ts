import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpService } from 'src/app/services/http-service.service';
import { Router } from '@angular/router';
import { StoreService } from 'src/app/services/store.service';

export enum Errors {
  NOTFOUND,
  EMPTY
};

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})

export class AuthComponent implements OnInit {
  
  private email : string = null;
  private authForm: FormGroup;
  private errors = Errors;
  private err = [false, false];
  constructor(private http: HttpService, private router: Router, private store: StoreService) { }

  ngOnInit() {
    this.authForm = new FormGroup({
      userName: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    });
    if (this.store.get('email')) {
      this.router.navigateByUrl('/content');
    }
    //this.authForm.setValue({userName: this.email, password: ''});
  }

  public clearErrors() {
    for (let x in this.err) {
      this.err[x] = false;
    }    
  }

  public login() {
    this.clearErrors();
    console.log('login: ');
    if (this.authForm.controls.userName.value == "" || this.authForm.controls.password.value == "") {
      console.log('empty auth form');
      this.err[Errors.EMPTY] = true;
      return ;
    }
    console.log(this.authForm.controls.userName.value);
    console.log(this.authForm.controls.password.value);
    let authBody = {
      userName: this.authForm.controls.userName.value,
      password: this.authForm.controls.password.value
    };
    this.http.post('/users/login', authBody).then((resp) => {
      console.log(resp);
      if (resp['success'] == true) {
        console.log('connected: ' + this.authForm.controls.userName.value);
        this.store.set('email', this.authForm.controls.userName.value);
        this.store.set('firstName', resp['firstName']);
        this.store.set('lastName', resp['lastName']);
        this.store.set('type', resp['type']);
        console.log('storing');
        console.log(this.store.get('type'));
        this.router.navigateByUrl('/content');
      } else {
        this.err[Errors.NOTFOUND] = true;
      }
    });
  }

}
