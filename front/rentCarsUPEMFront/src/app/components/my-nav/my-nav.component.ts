import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { StoreService } from 'src/app/services/store.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css'],
})
export class MyNavComponent implements OnInit {

  private name: string = "";
  private online: boolean = false;
  private hasNotif: boolean = false;
  private type: string = "EMPLOYEE";

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private notification: NotificationService, private breakpointObserver: BreakpointObserver, private store: StoreService, private router: Router) {
    console.log(this.store.get('firstName'));
    this.type = this.store.get('type') ? this.store.get('type') : "";
    if (this.store.get('firstName')) {
      this.name = this.store.get('firstName') + ' ' + this.store.get('lastName');
      this.online = true;
    }
    this.notification.notifObs.subscribe((notifs) => {
      console.log('nav notifs:');
      console.log(notifs);
      this.hasNotif = (notifs && notifs[0]);
    });
    this.store.storeObs.subscribe((storage) => {
      if (storage) {
        if (storage['type']) {
          this.type = storage['type'];
        }
        if (storage['firstName']) {
          this.name = storage['firstName'] + ' ' + storage['lastName'];
          this.online = true;
        } else if (!this.store.get('firstName')) {
          this.online = false;
          this.name = "";
        }
      }
    })
  }

  public logout() {
    this.store.clear();
    this.router.navigateByUrl('/auth');
  }
  
  ngOnInit() {

  }
}
