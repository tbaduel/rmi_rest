import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/http-service.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  public cars = [];
  private notifications: Object = null;
  constructor(private httpService: HttpService, private store: StoreService) {
    this.httpService.get('/users/getNotifs', {email: this.store.get('email')}).then((resp) => {
      this.notifications = resp;
    });
      this.httpService
        .get('/cars/getcars', []).then((resp) => {
          for(let val in resp) {
            this.cars.push(resp[val]);
          }
        });
      this.httpService.get('/buycar/getcars', {}).then((resp) => {
        console.log('UPEMCarsService:');
        console.log(resp);
      });
      
  }

  ngOnInit() {
  }

  public validateNotifications() {
    this.httpService.post('/users/removeNotifs', {email: this.store.get('email')}).then((resp) => {
      console.log(resp);
      this.notifications = null;
    });
  }
}
