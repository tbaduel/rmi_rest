import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() id: number;
  @Input() title: string;
  @Input() text: string;
  @Input() image: string;

  public state:  boolean;
  constructor() {
    this.state = false;
  }

  ngOnInit() {
  }

  public truncate(description: String) {
    return description.length > 90 && !this.state ? description.substr(0, 90) + '...' : description;
  }
}
