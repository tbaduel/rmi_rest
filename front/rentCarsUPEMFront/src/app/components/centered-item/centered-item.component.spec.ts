import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CenteredItemComponent } from './centered-item.component';

describe('CenteredItemComponent', () => {
  let component: CenteredItemComponent;
  let fixture: ComponentFixture<CenteredItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CenteredItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CenteredItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
