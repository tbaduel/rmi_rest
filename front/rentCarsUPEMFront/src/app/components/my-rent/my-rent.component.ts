import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http-service.service';
import { StoreService } from 'src/app/services/store.service';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-my-rent',
  templateUrl: './my-rent.component.html',
  styleUrls: ['./my-rent.component.css']
})
export class MyRentComponent implements OnInit {

  public hasCar: boolean = false;
  public car: any;
  constructor(private modal: NgxSmartModalService, private http: HttpService, private store: StoreService) {
    
  }

  public rateCar(text) {
    let mark = {
      carId: this.car['id'],
      carMark: this.car['rating'],
      stateMark: this.car['state'],
      opinion: text
    };
    console.log(mark);
    if (this.car && this.car.id) {
      this.http.post('/cars/addMark', mark)
        .then((data) => {
          console.log(data);
        });
    } else {
      console.log('empty car id');
    }
    this.giveBackCar();
  }

  public giveBackCar() {
    console.log('giving back car');
    if (this.car && this.car.id && this.store.get('email')) {
      this.http.post('/cars/release', {carId: this.car.id, empMail: this.store.get('email')})
        .then((data) => {
          if (data['isRent'] === true) {
            this.car = null;
            this.hasCar = false;
            this.store.set('hasCar', 'n');
          }
        });
    } else {
      console.log('empty car id or empty email');
    }
  }

  ratingCar(clickObj: any): void {
    this.car['rating'] = clickObj.rating;
  }
  ratingCarState(clickObj: any): void {
    this.car['state'] = clickObj.rating;
  }

  ngOnInit() {
    this.http.get('/users/getRental', {email: this.store.get('email')}).then((resp) => {
      console.log(resp);

      if (resp && resp['rental'] && resp['rental']['car']) {
        this.hasCar = true;
        this.car = resp['rental']['car'];
        this.car['rating'] = 3;
        this.car['state'] = 3;
      }
    });
  }

  public openModal() {
    this.modal.open('releaseModal');
  }

}
