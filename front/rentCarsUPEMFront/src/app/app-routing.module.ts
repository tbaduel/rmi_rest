import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { MyDashboardComponent } from './components/my-dashboard/my-dashboard.component';
import { CarComponent } from './components/car/car.component';
import { AuthComponent} from './components/auth/auth.component';
import { ContentComponent } from './components/content/content.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { MyRentComponent } from './components/my-rent/my-rent.component';

const routes: Routes = [
  { path: '', redirectTo: '/auth', pathMatch: 'full' },
  { path: 'auth', component: AuthComponent},
  { path: 'car/:id', component: CarComponent, canActivate: [AuthGuard] },
  { path: 'content', component: ContentComponent, canActivate: [AuthGuard]},
  { path: 'myrent', component: MyRentComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '/auth'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {


}
