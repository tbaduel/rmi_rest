import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { StorageServiceModule } from 'angular-webstorage-service';
import { HeaderComponent } from './components/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './components/my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDashboardComponent } from './components/my-dashboard/my-dashboard.component';
import { MyTableComponent } from './components/my-table/my-table.component';
import { ContentComponent } from './components/content/content.component';
import { CardComponent } from './components/card/card.component';
import { CarComponent } from './components/car/car.component';
import { TagComponent } from './components/tag/tag.component';
import { AuthComponent } from './components/auth/auth.component';
import { MyRentComponent } from './components/my-rent/my-rent.component';
import { CenteredItemComponent } from './components/centered-item/centered-item.component';
import { RatingComponent } from './components/rating/rating.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MyNavComponent,
    MyDashboardComponent,
    MyTableComponent,
    ContentComponent,
    CardComponent,
    CarComponent,
    TagComponent,
    AuthComponent,
    MyRentComponent,
    CenteredItemComponent,
    RatingComponent,
  ],
  imports: [
    NgxSmartModalModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    StorageServiceModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
