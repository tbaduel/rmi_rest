package fr.umlv.rmi;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.List;

public class CarServer {

	
	
	 public static void main(String[] args) {
		 String descriptionClio = "Cela fait longtemps qu'on ne présente plus la Renault Clio : lancée depuis 1990, la citadine du constructeur français s'est érigée au fil de ses versions en tant que valeur sûre de son segment. Pour la location d’une petite voiture polyvalente, la flotte automobile de UPEM Corp est dotée de la Renault Clio, mais vous pourrez également la louer à l'international via le formulaire de réservation ci-dessous à des tarifs qui pourraient bien vous étonner !";
	     String descriptionAstra = "Depuis son lancement en 1991, l'Opel Astra est rapidement devenue une valeur sûre du segment des moyennes. Disponible en version berline, break (Astra Sports Tourer) et Astra GTC en ce qui concerne la version de 2009, cette 4e mouture de l'Opel Astra est commercialisée à travers le monde sous plusieurs appellations (Vauxhall Astra en Grande-Bretagne, Buick Excelle en Chine, Buick Verano en Amérique du Nord, etc.). Chez UPEM Corp, vous trouverez l'Opel Astra à la location aussi bien en France qu'à l'étranger.";
	     String descriptionTwingo = "Apparue sur les routes en 1993, la Renault Twingo s'est rapidement imposée dans nos agglomérations urbaines pour devenir l'un des succès de la marque française. UPEM Corp dispose d’une importante variété de modèles de différentes marques pour votre location de citadine. La Renault Twingo vous est proposée dans de nombreuses agences en Europe : vérifiez sa disponibilité et les tarifs en utilisant notre formulaire en ligne ci-dessous.";
	     String descriptionClasseA = "La Mercedes Classe A ne néglige certainement pas le plaisir de conduite. Son agréable vivacité et son comportement routier rassurant sont également surprenants pour une berline compacte. La Mercedes Classe A est en effet dotée d’un train de roulement Sport de série. Sous le capot, on retrouve toute une série de moteurs essence et diesels dont la puissance et la flexibilité sont amplement suffisantes pour propulser un véhicule du gabarit de la Classe A tout en offrant des chiffres de consommation bien plus que corrects.";
	     String descriptionHuracan= "Louer une voiture de luxe est un plaisir que l’on savoure à chaque fois. Mais la location d’une Lamborghini Huracan vous fera vivre des sensations encore plus fortes. Imaginez-vous au volant de cette sportive toute récente. Et si vous arrêtiez de rêver et que vous contactiez nos équipes pour réserver cette voiture d’exception le temps d’une journée ou plus ? Que ce soit pour une occasion spéciale comme un mariage ou juste pour vous faire plaisir, vous ne regretterez pas d’avoir loué ce coupé de légende.";
	     String descriptionPanamera = "La Porsche Panamera, une berline 4 portes de type propulsion, a été commercialisée en 2011 afin de concurrencer des véhicules comme la Maserati Quattroporte. Les ingénieurs de Porsche s'étaient lancé un défi audacieux : proposer à sa clientèle un véhicule polyvalent, à la fois éminemment sportif et pratique pour les besoins du quotidien. Pari réussi pour ce fruit de la stratégie de diversification de Porsche.";
	     String description488gtb = "La 488 GTB revient aux sources du modèle d'origine, 488 faisant référence au déplacement unitaire du moteur et GTB à « Gran Turismo Berlinetta ». La nouvelle voiture délivre non seulement des performances incomparables, mais également une puissance exploitable et contrôlable, même par les pilotes les moins expérimentés.";
	     String descriptionDS3 = "Avec la gamme DS, Citroën s'est installé durablement sur le segment du premium. Certes, la marque aux chevrons a pour ambition d'atteindre la qualité des meilleures allemandes, mais tout en capitalisant sur ces points forts pour proposer une version française de ce Premium. Découvrez toutes les qualités de la Citroën DS3 via une location dans le réseau UPEM Corp : cette compacte disponible aux 4 coins de l'Hexagone, louez-la sans plus tarder à des prix compétitifs !";
	     String descriptionBravo = "Arrivée en 2007 dans la gamme du constructeur italien, la Fiat Bravo est une berline compacte qui est apparue en même temps que la Brava. En Europe, la production a été interrompue en 2014 sans qu'aucun remplaçant n'ait été annoncé. La Fiat Bravo est toujours produite au Brésil, où elle est commercialisée depuis 2010. Louez votre Fiat Bravo tant qu'il en est encore temps avec Sixt et profitez de notre rapport qualité-prix exceptionnel.";
	     String description308 = "À la recherche d'une location de Peugeot 308, la berline audacieuse de la marque au lion ? UPEM Corp la propose dans sa flotte en France et en Europe. La première qualité de la Peugeot 308 est son comportement routier. Si elle est parfaite pour les trajets les plus longs, elle vous offre aussi un poste de conduite moderne, épuré, ergonomique. C’est un véritable cockpit pourvu d’une tablette tactile de 9,7’’et d’un volant de diamètre réduit.";
	     String descriptionC4 = "Berline compacte confortable et sécurisante, la Citroën C4 a un design extérieur affirmé tout en étant volontairement sage si bien qu’elle dégage un sentiment de solidité et de fiabilité. La C4 est déclinée en plusieurs versions (Cactus, Grand Picasso), disponibles à la location en France et à l'étranger. Plusieurs détails viennent apporter une délicate touche de sophistication. Ce choix stylistique permet de mettre en lumière le principal point fort de la Citroën C4 : l’excellence du confort.";
	     String descriptionZoe = "Renault a présenté la première Zoé au Mondial de l’automobile de Paris en 2010. Produite en banlieue parisienne, sa commercialisation a démarré en mars 2013. Sur sa dernière version, la Renault Zoé R240, le constructeur au losange a amélioré l’autonomie de son modèle et diminué le temps de recharge, des paramètres qui sont en progrès constant dans le secteur de la voiture électrique. Participez au respect de l’environnement avec Sixt en louant l’un des véhicules hybrides et électriques disponibles chez UPEM Corp.";
	     String descriptionPrius = "En 1997, Toyota révolutionnait le monde de l'automobile de masse, qui reposait exclusivement depuis plus de 100 ans sur le moteur à explosion, en proposant le premier véhicule hybride grand public au Japon. Trois ans plus tard, juste à temps pour l'arrivée du nouveau millénaire, la Toyota Prius était disponible à travers le monde. Retrouvez ce précurseur à louer dans certaines agences UPEM Corp des 4 coins du globe à des tarifs toujours compétitifs.";
	     String descriptionMustang = "La Ford Mustang se distingue par son design sportif dès sa naissance en 1964. Après quelques relookings dans les années 1990 puis 2000, surfant sur les tendances néo-rétro, la Mustang fait toujours loi sur les routes américaines : vous aussi, prenez le volant de cette voiture indétrônable grâce à UPEM Corp location de voitures.";
	     String descriptionPolo = "La Volkswagen Polo, arrivée sur le marché en 1975 dans la foulée de la Golf, est depuis l'une des valeurs sûres du segment B en Europe. À chacune de ses moutures, la petite citadine n'a cessé de gagner en largeur, longueur et hauteur. UPEM Corp vous propose de louer la Volkswagen Polo en France ainsi que sur de nombreuses destinations populaires d’Europe, comme en Espagne, à des tarifs défiant toute concurrence.";
	     System.setProperty("java.security.policy","file:./security.policy");
         System.setProperty("java.rmi.server.hostname", "localhost");
	     try {
	        	// A ajouter si besoin
	            Renter renter = new RenterImpl();
	            renter.addCar(1, "Clio 4", descriptionClio, Color.RED, EngineType.GASOLINE, 200, "Renault", BigDecimal.valueOf(400L),  BigDecimal.valueOf(8000L), "clio4.jpeg");
	            renter.addCar(2, "Twingo 2", descriptionAstra, Color.RED, EngineType.GASOLINE, 100, "Renault", BigDecimal.valueOf(150L), BigDecimal.valueOf(6000L), "twingo2.jpg");
	            renter.addCar(3, "Astra", descriptionTwingo, Color.RED, EngineType.GASOLINE, 300, "Opel", BigDecimal.valueOf(500L), BigDecimal.valueOf(8000L), "astra.jpg");
	            renter.addCar(4, "Classe A", descriptionClasseA, Color.GREY, EngineType.DIESEL, 450, "Mercedes", BigDecimal.valueOf(750L), BigDecimal.valueOf(25000L), "classeA.jpg");
	            renter.addCar(5, "Huracan", descriptionHuracan, Color.RED, EngineType.GASOLINE, 700, "Lamborghini", BigDecimal.valueOf(1800L), BigDecimal.valueOf(90000L), "huracan.jpg");
	            renter.addCar(6, "Panamera", descriptionPanamera, Color.BLACK, EngineType.GASOLINE, 500, "Porsche", BigDecimal.valueOf(800L), BigDecimal.valueOf(45000L), "panamera.jpg");
	            renter.addCar(7, "488 Gtb", description488gtb, Color.YELLOW, EngineType.GASOLINE, 680, "Ferrari", BigDecimal.valueOf(1600L), BigDecimal.valueOf(75000L), "488gtb.jpg");
	            renter.addCar(8, "DS3", descriptionDS3, Color.RED, EngineType.GASOLINE, 680, "Citroen", BigDecimal.valueOf(200L), BigDecimal.valueOf(20000L), "ds3.png");
	            renter.addCar(9, "Fiat Bravo", descriptionBravo, Color.RED, EngineType.GASOLINE, 100, "Fiat", BigDecimal.valueOf(250L), BigDecimal.valueOf(20000L), "bravo.jpg");
	            renter.addCar(10, "308", description308, Color.WHITE, EngineType.DIESEL, 140, "Peugeot", BigDecimal.valueOf(225L), BigDecimal.valueOf(18000L), "308.png");
	            renter.addCar(11, "C4", descriptionC4, Color.WHITE, EngineType.DIESEL, 100, "Citroen", BigDecimal.valueOf(200L), BigDecimal.valueOf(15000L), "C4.png");
	            renter.addCar(12, "Zoe", descriptionZoe, Color.BLACK, EngineType.ELETRIC, 90, "Reunault", BigDecimal.valueOf(250L), BigDecimal.valueOf(17500L), "Zoe.jpg");
	            renter.addCar(13, "Prius", descriptionPrius, Color.WHITE, EngineType.ELETRIC, 120, "Toyota", BigDecimal.valueOf(300L), BigDecimal.valueOf(30000L), "prius.png");
	            renter.addCar(14, "Mustang Cabriolet", descriptionMustang, Color.RED, EngineType.GASOLINE, 400, "Ford", BigDecimal.valueOf(750L), BigDecimal.valueOf(60000L), "mustang-cabriolet.png");
	            renter.addCar(15, "Mustang Coupé", descriptionMustang, Color.RED, EngineType.GASOLINE, 400, "Ford", BigDecimal.valueOf(750L), BigDecimal.valueOf(60000L), "mustang-coupe.png");
	            renter.addCar(16, "Polo", descriptionPolo, Color.GREY, EngineType.GASOLINE, 140, "Volkswagen ", BigDecimal.valueOf(300L), BigDecimal.valueOf(20000L), "polo.png");
	            renter.addEmployee((User)Naming.lookup("fabien"));
	            renter.addEmployee((User)Naming.lookup("ichem"));
	            renter.addEmployee((User)Naming.lookup("alan"));
	            renter.addEmployee((User)Naming.lookup("thomas"));
	            renter.addEmployee((User)Naming.lookup("buyer"));
	            System.out.println(renter.getEmployee());
	            Naming.rebind("Renter", renter);
	        } catch (RemoteException | MalformedURLException | NotBoundException e) {
	            System.out.println("Error : " + e);

	        }
	 }
}
