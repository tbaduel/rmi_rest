package fr.umlv.rmi;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CarImpl extends UnicastRemoteObject implements Car{
	
	private long id;
	private String model;
	private String description;
	private List<Mark> marks;
	private Queue<User> pendingUserQueue;
	private Color color;
	private EngineType engineType;
	private int enginePower;
	private BigDecimal priceBuy;
	private BigDecimal priceRent;
	private int rentalsCount;
	private String image;
	private User currentUser;
	
	public CarImpl(long id, String model, String description, Color color, EngineType engineType, int enginePower, BigDecimal priceRent, BigDecimal priceBuy, String image, User currentUser) throws RemoteException {
		super();
		this.id = id;
		this.model = model;
		this.description = description;
		this.color = color;
		this.engineType = engineType;
		this.enginePower = enginePower;
		this.priceRent = priceRent;
		this.priceBuy = priceBuy;
		this.image = image;
		this.currentUser = currentUser;
		
		this.marks = new ArrayList<>();
		this.pendingUserQueue = new LinkedList<>();
		
	}

	@Override	
	public String getModel() {
		return model;
	}

	@Override
	public String getDescription() throws RemoteException{
		return description;
	}

	@Override
	public List<Mark> getMarks() {
		return marks;
	}

	@Override
	public Queue<User> getPendingUserQueue() {
		return pendingUserQueue;
	}

	@Override
	public Color getColor() {
		return this.color;
	}

	@Override
	public EngineType getEngineType() {
		return engineType;
	}

	@Override
	public int getEnginePower() {
		return enginePower;
	}

	@Override
	public BigDecimal getPriceRent() {
		return priceRent;
	}
	
	@Override
	public BigDecimal getPriceBuy() {
		return priceBuy;
	}
	
	@Override
	public String getImage() {
		return image;
	}

	@Override
	public int getRentalsCount() {
		return rentalsCount;
	}
	
	@Override
	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + enginePower;
		result = prime * result + ((engineType == null) ? 0 : engineType.hashCode());
		result = prime * result + ((marks == null) ? 0 : marks.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((priceRent == null) ? 0 : priceRent.hashCode());
		result = prime * result + ((priceBuy == null) ? 0 : priceBuy.hashCode());
		result = prime * result + ((pendingUserQueue == null) ? 0 : pendingUserQueue.hashCode());
		result = prime * result + rentalsCount;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!super.equals(obj))
			return false;
		CarImpl other = (CarImpl) obj;
		return this.model == other.model
				&& this.description.equals(other.description)
				&& this.color == other.color
				&& this.engineType == other.engineType
				&& this.enginePower == other.enginePower
				&& this.priceRent == other.priceRent
				&& this.priceBuy == other.priceBuy
				&& this.rentalsCount == other.rentalsCount;
	}
	
	@Override
	public void rent(User user) throws RemoteException{
		this.rentalsCount += 1;
		this.currentUser = user;
	}
	
	@Override
	public void addPendingUser(User other) throws RemoteException {
		for(User user : pendingUserQueue) {
			if(user.isEqualTo(other)) {
				return;
			}
		}
		
		pendingUserQueue.add(other);
	}

	@Override
	public void addMark(int carkMark, int stateMark, String opinion) throws RemoteException {
		marks.add(new MarkImpl(carkMark, stateMark, opinion));
		
	}

	@Override
	public User getCurrentUser() throws RemoteException {
		return currentUser;
	}	
	

}
