package fr.umlv.rmi;

import java.math.BigDecimal;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Optional;

public interface Renter extends Remote{
	
	
	public List<Car> getCarList() throws RemoteException;
	public List<Car> getFreeCar() throws RemoteException;
	public boolean rentACar(long carId, String employeeEmail) throws RemoteException;
	public boolean releaseACar(long carId) throws RemoteException;
	public void addCar(long id, String model, String description, Color color, EngineType engineType, int enginePower, String brand, BigDecimal priceRent, BigDecimal priceBuy, String image) throws RemoteException;
	public void removeCar(long id) throws RemoteException;
	public void addEmployee(User user) throws RemoteException;
	public List<User> getEmployee() throws RemoteException;
	public User authentication(String email, String password) throws RemoteException;
	public Car findCarById(long id) throws RemoteException;
	public User findEmployeeByEmail(String email) throws RemoteException;
	public boolean buyACar(long carId, String clientEmail) throws RemoteException;
	
}
