package fr.umlv.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Mark extends Remote{
	public int getCarMark() throws RemoteException;
	public int getStateMark() throws RemoteException;
	public String getOpinion() throws RemoteException;
}
