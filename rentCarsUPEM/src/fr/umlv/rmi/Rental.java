package fr.umlv.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

public interface Rental extends Remote{
	
	public Car getCar() throws RemoteException;

	public User getUser() throws RemoteException;

	public Date getStartDate() throws RemoteException;

	public Date getEndDate() throws RemoteException;
}
