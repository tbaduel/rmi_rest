package fr.umlv.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

public class RentalImpl extends UnicastRemoteObject implements Rental{
	
	private Car car;
	private User user;
	private Date startDate;
	private Date endDate;
	
	public RentalImpl(Car car, User user, Date startDate, Date endDate) throws RemoteException {
		super();
		this.car = car;
		this.user = user;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Car getCar() {
		return car;
	}

	public User getUser() {
		return user;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!super.equals(obj))
			return false;
		RentalImpl other = (RentalImpl) obj;
		return this.car.equals(other.getCar())
				&& this.user.equals(other.getUser())
				&& this.startDate.equals(other.getStartDate())
				&& this.endDate.equals(other.getEndDate());
	}
	
	

}
