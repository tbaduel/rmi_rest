package fr.umlv.rmi;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class RenterImpl extends UnicastRemoteObject implements Renter{

	private static final long serialVersionUID = 1L;
	
	
	/* Ne serait il pas mieux d'avoir la liste des véhicules dans une hashmap,
	 * avec comme clé si la voiture est dispo ou pas
	 * 
	 */
	
	private List<Car> carList;
	private List<Rental> rentalList;
	private List<User> employeeList;
	
	protected RenterImpl() throws RemoteException {
		super();
		rentalList = new ArrayList<>();
		carList = new ArrayList<>();
		employeeList = new ArrayList<>();
	}
	
	@Override
	public void addCar(long id, String model, String description, Color color, EngineType engineType, int enginePower, String brand, BigDecimal priceRent, BigDecimal priceBuy, String image) throws RemoteException {
		carList.add(new CarImpl(id, model, description, color, engineType, enginePower, priceRent,priceBuy, image, null));	
	}
	
	@Override
	public void addEmployee(User user) throws RemoteException {
		employeeList.add(user);
	}
	
	@Override
	public List<Car> getCarList() throws RemoteException {
		return carList;
	}
	
	@Override
	public List<Car> getFreeCar() throws RemoteException{
		ArrayList<Car> listCars = new ArrayList<>();
		return listCars;
		
	}
	
	public Date computeEndDate(Date startDate) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 6);
		return cal.getTime();
	}
	
	@Override
	public boolean rentACar(long carId, String employeeEmail) throws RemoteException {
		Car car;
		User user;
		
		try{
			car = findCarById(carId);
			user = findEmployeeByEmail(employeeEmail);
		}
		catch(NoSuchElementException e) {
			throw new NoSuchElementException("User or car not found");
		}
		
		if(isRent(car)) {
			car.addPendingUser(user);
			return false;
		}
		
		if(user.getCurrentRental() != null) {
			return false;
		}
		
		Date startDate = new Date();
		
		Rental rental = new RentalImpl(car, user, startDate, computeEndDate(startDate));
		rentalList.add(rental);
		
		car.rent(user);
		user.setCurrentRental(rental);
		
		return true;
		
	}

	@Override
	public boolean releaseACar(long carId) throws RemoteException {
		Rental rental;
		
		try {
			rental = findRentalByCarId(carId);
		}
		catch(NoSuchElementException e) {
			return false;
		}
		
		Car car = rental.getCar();
		User currentRenter = rental.getUser();
		
		rentalList.remove(rental);
		
		if(!car.getPendingUserQueue().isEmpty()) {
			User nextRenter = car.getPendingUserQueue().remove();
			
			Date startDate = new Date();
			Rental newRental = new RentalImpl(car, nextRenter, startDate, computeEndDate(startDate));
			rentalList.add(newRental);
			
			nextRenter.addNotification("Nouvelle voiture louée", String.format("La voiture %s pour laquelle vous étiez en attente vous a été attribuée", car.getModel()));
			nextRenter.setCurrentRental(newRental);
			
		}
		
		currentRenter.setCurrentRental(null);
		return true;
		
	}
	
	
	public boolean isRent(Car car) throws RemoteException {
		for(Rental rental : rentalList) {
			if(rental.getCar().equals(car)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public User findEmployeeByEmail(String email) {
		for(User employee : employeeList) {
			
			try {
				System.out.println("mail : " + email + " != " + employee.getEmail() + " ?");
				if(employee.getEmail().equals(email)) {
					System.out.println(employee.getEmail());
					return employee;
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		
		throw new NoSuchElementException("Employee not found");
	}
	
	public Car findCarById(long id) {
		for(Car car : carList) {
			try {
				if(car.getId() == id) {
					return car;
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		
		throw new NoSuchElementException("Car not found");
	}
	
	public Rental findRentalByCarId(long id) {
		for(Rental rental : rentalList) {
			try {
				if(rental.getCar().getId() == id) {
					return rental;
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		
		throw new NoSuchElementException("Car not found");
	}
	
	@Override
	public User authentication(String email, String password) throws RemoteException {
		User user;
		try {
			user = findEmployeeByEmail(email);
			if(user.getPassword().equals(password)) {
				return user;
			}
			return null;
		}
		catch(NoSuchElementException | RemoteException e) {
			return null;
		}
		
	}
	
	@Override
	public void removeCar(long id) {
		Car car = findCarById(id);
		carList.remove(car);
	}

	@Override
	public List<User> getEmployee() throws RemoteException {
		return employeeList;
		
	}
	
	@Override
	public boolean buyACar(long carId, String clientEmail) throws RemoteException{
		Car car;
		User user;
		System.out.println("Buying ...\ncarID = " + carId + ", client = " + clientEmail);
		try{
			car = findCarById(carId);
			user = findEmployeeByEmail(clientEmail);
		}
		catch(NoSuchElementException e) {
			throw new NoSuchElementException("User or car not found");
		}
		
		if(isRent(car)) {
			return false;
		}
		
		
		carList.remove(car);
		
		
		return true;
		
	}
	
	/*
	private User findClientByEmail(String clientEmail) {
		User user;
		
		
	}
	*/
	
}
