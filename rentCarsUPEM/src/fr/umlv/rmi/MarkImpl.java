package fr.umlv.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MarkImpl extends UnicastRemoteObject implements Mark{
	private int carMark;
	private int stateMark;
	private String opinion;
	
	public MarkImpl(int carkMark, int stateMark, String opinion) throws RemoteException {
		super();
		this.carMark = carkMark;
		this.stateMark = stateMark;
		this.opinion = opinion;
	}

	public int getCarMark() throws RemoteException {
		return carMark;
	}

	public int getStateMark() throws RemoteException {
		return stateMark;
	}

	public String getOpinion() throws RemoteException {
		return opinion;
	}
	
	
	
	
}
