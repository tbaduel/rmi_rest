package fr.umlv.upemCarsService.cars;


import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.umlv.rmi.Car;
import fr.umlv.rmi.Mark;
import fr.umlv.rmi.Renter;
import fr.umlv.upemCarsService.json.CarBuy;
import fr.umlv.upemCarsService.json.UserWalletCurrencyJSON;
import fr.umlv.upemCarsService.json.WalletWithCurrency;

@Path("/buycar")
public class CarServices{
	

	
	@GET
	@Path("/car/{carid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCarInfo(@PathParam("carid")long carId) {
		JsonObjectBuilder carsBuilder = Json.createObjectBuilder();
		Renter renter;
		try {
			renter = (Renter) Naming.lookup("Renter");
			List<Car> listcar = renter.getCarList();
			
			Optional<Car> Optcar = getCarFromList(listcar,carId);
			if (Optcar.isPresent()) {
				Car car = Optcar.get();
				carsBuilder.add(Long.toString(car.getId()),getCarJson(car));
			}	
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("getCarInfo");
			e.printStackTrace();
		}
	
		JsonObject carsJsonObject = carsBuilder.build();
		return carsJsonObject.toString();
	}
	
	
	@POST
	@Path("/buy")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String buyACar(CarBuy clientAuth) {
		System.out.println("ok buy a car:" + clientAuth.getCarId());
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		Renter renter;
		UserWalletCurrencyJSON uwc;
		try {
			renter = (Renter) Naming.lookup("Renter");
			List<Car> carlist = renter.getCarList();
			System.out.println("id: " + clientAuth.getCarId());
			Optional<Car> optCar = getCarFromList(carlist, clientAuth.getCarId() );
			System.out.println(optCar.get().getModel());
			if(optCar.isPresent()) {
				System.out.println("here");
				Car car = optCar.get();
				BigDecimal valueNotConverted = car.getPriceBuy();
				
				System.out.println("here2: " + clientAuth.getEmpMail());
				WalletWithCurrency wallet = getJsonWalletFromUser(clientAuth.getEmpMail());
				System.out.println("here3:" + wallet);
				System.out.println("here4:" + wallet.getValue());
				BigDecimal value = getValueConvert(valueNotConverted, wallet.getCurrency(), clientAuth.getCurrency());
				System.out.println("New value = " + value);
				if (wallet.getValue().compareTo(value) == 1) {
					uwc = new UserWalletCurrencyJSON();
					uwc.setCurrency(wallet.getCurrency());
					uwc.setUserEmail(clientAuth.getEmpMail());
					uwc.setValue(value);
					System.out.println("transaction = " + getJsonTransaction(uwc));
					Boolean isBuy = renter.buyACar(car.getId(), clientAuth.getEmpMail());
					responseBuilder.add("isBuy", isBuy);
				}
			}
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("rentACar");
			e.printStackTrace();
		}
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	

	
	@GET
	@Path("/getcars")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSaleCars() {
		JsonObjectBuilder carsBuilder = Json.createObjectBuilder();
		try {
			Renter renter = (Renter) Naming.lookup("Renter");
			List<Car> listcar = renter.getCarList();		
			for (Car car : listcar) {
				if (car.getRentalsCount() > 0) {
					carsBuilder.add(Long.toString(car.getId()),getCarJson(car));
				}
			}
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("getSaleCars");
			e.printStackTrace();
	
		}
		JsonObject carsJsonObject = carsBuilder.build();
		return carsJsonObject.toString();
	}
	
	
	
		
	private Optional<Car> getCarFromList(List<Car> listcar, long carId) {
		for (Car car : listcar) {
			try {
				System.out.println(car.getId());
				if (car.getId() == carId) {
					return Optional.of(car);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return Optional.empty();
			
	}
	
	
	private JsonObjectBuilder getCarJson(Car car) {
		JsonObjectBuilder carBuilder = Json.createObjectBuilder();
		try {
			JsonArray JsonMarkArray = getJsonMarkArray(car.getMarks());
			carBuilder.add("marks", JsonMarkArray);
			carBuilder.add("id", car.getId());
			carBuilder.add("model",car.getModel());
			carBuilder.add("description",car.getDescription());
			carBuilder.add("color",car.getColor().toString());
			carBuilder.add("engine",car.getEngineType().toString());
			carBuilder.add("power",car.getEnginePower());
			carBuilder.add("price",car.getPriceBuy());
			carBuilder.add("image", car.getImage());
			carBuilder.add("pendingUsers", car.getPendingUserQueue().size());
			carBuilder.add("currentRenter", car.getCurrentUser() != null ? car.getCurrentUser().getEmail() : "");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return carBuilder;
	
	}
	
	private JsonArray getJsonMarkArray(List<Mark> carMarks) throws RemoteException {
		JsonArrayBuilder carMarksBuilder = Json.createArrayBuilder();
		for(Mark mark : carMarks) {
			JsonObjectBuilder markJson = Json.createObjectBuilder();
			markJson.add("carMark", mark.getCarMark());
			markJson.add("stateMark", mark.getStateMark());
			markJson.add("opinion", mark.getOpinion());
			carMarksBuilder.add(markJson.build());
		}
		return carMarksBuilder.build();
	}
	
	public WalletWithCurrency getJsonWalletFromUser(String userEmail) {
        Client client = ClientBuilder.newClient();
        return client
          .target("http://localhost:8095/BankService/rest/bank/getWallet")
          .queryParam("user", userEmail)
          .request(MediaType.APPLICATION_JSON)
          .get(WalletWithCurrency.class);
    }

	
	
	public Response getJsonTransaction(UserWalletCurrencyJSON usr) {
		Client client = ClientBuilder.newClient();
	    return client
	      .target("http://localhost:8095/BankService/rest/bank/removeMoney")
	      .request(MediaType.APPLICATION_JSON)
	      .post(Entity.entity(usr, MediaType.APPLICATION_JSON));
	}
	
	
	
	public BigDecimal getValueConvert(BigDecimal value, String currencyCurrent, String currencyWanted) {
		JsonObject jo = getJsonConvert(currencyWanted);
		System.out.println("--------------------------");
		System.out.println(currencyCurrent);
		System.out.println(jo.toString());
		BigDecimal val = BigDecimal.valueOf(Double.valueOf(((JsonObject)jo.get("rates")).get(currencyCurrent).toString()));
		System.out.println("New value = " + value.multiply(val));
		return value.multiply(val);
		
	}
	
	public JsonObject getJsonConvert(String currencyBase) {
		Client client = ClientBuilder.newClient();
        return client
          .target("https://api.exchangeratesapi.io/latest?base=" + currencyBase)
          .request(MediaType.APPLICATION_JSON)
          .get(JsonObject.class);
				
	}

}