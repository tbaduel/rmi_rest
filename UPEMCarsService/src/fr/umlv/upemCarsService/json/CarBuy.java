package fr.umlv.upemCarsService.json;

public class CarBuy {

	
	private long carId;
	private String empMail;
	private String currency;
	
	public CarBuy() {
		
	}
	
	public long getCarId() {
		return carId;
	}
	public void setCarId(long carId) {
		this.carId = carId;
	}
	public String getEmpMail() {
		return empMail;
	}
	public void setEmpMail(String empMail) {
		this.empMail = empMail;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
}
