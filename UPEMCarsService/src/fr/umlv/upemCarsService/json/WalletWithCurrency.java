package fr.umlv.upemCarsService.json;

import java.math.BigDecimal;

public class WalletWithCurrency {
	private BigDecimal value;
	private String currency;
	
	
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}
