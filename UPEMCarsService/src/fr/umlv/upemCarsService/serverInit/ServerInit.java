package fr.umlv.upemCarsService.serverInit;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.umlv.rmi.Car;
import fr.umlv.rmi.Renter;
import fr.umlv.rmi.User;
import fr.umlv.rmi.UserType;
import fr.umlv.upemCarsService.json.UserWalletCurrencyJSON;


/**
 * Servlet implementation class ServerInit
 */
@WebServlet("/ServerInit")
public class ServerInit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * Default constructor. 
     */
    public ServerInit() {
        
    	
		try {
			// On recupere renter
			//LocateRegistry.createRegistry(1099);
			System.setProperty("java.security.policy","file:./security.policy");		
			Renter renter = (Renter) Naming.lookup("Renter");
			List<Car> cars = renter.getCarList();
			for (Car car : cars) {
				System.out.println("car = " + car.getModel());
			}
			
			List<User> users = renter.getEmployee();
			for (User user : users) {
				if (user.getUserType().equals(UserType.CUSTOMER)) {
					UserWalletCurrencyJSON usr = new UserWalletCurrencyJSON();
					usr.setUserEmail(user.getEmail());
					usr.setValue(BigDecimal.valueOf(15000));
					usr.setCurrency("USD");
					createJsonWalletForUser(usr);
				}
			}
			
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("ServerInit");
			e.printStackTrace();

		}
    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	
	public Response createJsonWalletForUser(UserWalletCurrencyJSON usr) {
		Client client = ClientBuilder.newClient();
	    return client
	      .target("http://localhost:8095/BankService/rest/bank/addUserAccount")
	      .request(MediaType.APPLICATION_JSON)
	      .post(Entity.entity(usr, MediaType.APPLICATION_JSON));
	}

}
