package fr.umlv.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class UserImpl extends UnicastRemoteObject implements User {
	private String name;
	private String firstName;
	private String password;
	private String email;
	private UserType userType;
	private List<Notification> notifications;
	private Rental currentRental;
	BankAccount bankAccount;
	
	public UserImpl(String name, String firstName, String password, String email, Rental currentRental, BankAccount bankAccount, UserType userType) throws RemoteException {
		super();
		this.name = name;
		this.firstName = firstName;
		this.password = password;
		this.email = email;
		this.currentRental = currentRental;
		this.bankAccount = bankAccount;
		this.userType = userType;
		this.notifications = new ArrayList<Notification>();
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getFirstName() {
		return firstName;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	@Override
	public String getEmail() {
		return email;
	}
	
	@Override
	public List<Notification> getNotifications() {
		return notifications;
	}
	
	@Override
	public Rental getCurrentRental() {
		return currentRental;
	}
	
	@Override
	public void setCurrentRental(Rental rental) {
		this.currentRental = rental;
	}
	
	@Override
	public BankAccount getBankAccount() {
		return bankAccount;
	}
	
	@Override
	public UserType getUserType() {
		return userType;
	}
	
	@Override
	public void addNotification(String type, String message) throws RemoteException {
		notifications.add(new NotificationImpl(type, message));
	}
	
	@Override
	public boolean isEqualTo(User other) throws RemoteException {
		return this.name.equals(other.getName())
				&& this.firstName.equals(other.getFirstName())
				&& this.password.equals(other.getPassword())
				&& this.email.equals(other.getEmail());
	}

	@Override
	public void removeNotifications() throws RemoteException {
		if (this.notifications != null) {
			this.notifications.clear();
		}
	}
	
	
	
	
}
