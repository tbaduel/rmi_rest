package fr.umlv.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class NotificationImpl extends UnicastRemoteObject implements Notification {
	private String type;
	private String message;
	
	public NotificationImpl(String type, String message) throws RemoteException {
		super();
		this.type = type;
		this.message = message;
	}
	
	@Override
	public String getType() {
		return type;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	
	
	
}
