package fr.umlv.rmi;

import java.math.BigDecimal;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Queue;

public interface Car extends Remote{
	
	public String getModel() throws RemoteException;
	public String getDescription() throws RemoteException;
	public List<Mark> getMarks() throws RemoteException;
	public Queue<User> getPendingUserQueue() throws RemoteException;
	public Color getColor() throws RemoteException;
	public EngineType getEngineType() throws RemoteException;
	public int getEnginePower() throws RemoteException;
	public BigDecimal getPriceRent() throws RemoteException;
	public BigDecimal getPriceBuy() throws RemoteException;
	public int getRentalsCount() throws RemoteException;
	public long getId() throws RemoteException;
	public String getImage() throws RemoteException;
	public void rent(User user) throws RemoteException;
	public void addPendingUser(User other) throws RemoteException;
	public void addMark(int carkMark, int stateMark, String opinion) throws RemoteException;
	public User getCurrentUser() throws RemoteException;
}
