# Installation du projet
(Back JAVA / JAXRS-RI (Jersey) / TOMCAT 7)

- R�cup�rer les sources sur (GitLab et cloner le projet), ou le dossier de rendu
- Ouvrir le workspace dans Eclipse
- Importer les 4 projets
- Laisser Maven build les projets



Il faut pour le projet faire tourner les diff�rents services sur des jvm diff�rentes. Pour cela, nous allons avoir une jvm et trois 
serveurs sous Tomcat 7.

- Ajouter un premier serveur Tomcat (Onglet � Servers � => Create�)
	* Dans la fen�tre nommer le Tomcat � RentCarsUpem �
	* Ajouter le projet � RentCarsUpem � (Attention pas le deuxi�me)
	* Valider
-	Ajouter un deuxi�me serveur Tomcat (Clic droit dans l�onglet Servers => New => Server)
	* Saisir � UPEMCarsService � pour le nom
	* Ajouter le projet � UPEMCarsService �
	* Valider
	* Dans l�onglet � Server �, double cliquer sur le serveur � UPEMCarsService �
	* Dans l�onglet port, modifier les diff�rents ports :
		* Tomcat admin port : 8006
		* HTTP/1.1 : 8090
		* AJP/1.3 : 8010
		
- Ajouter un troisieme serveur Tomcat (Clic droit dans l�onglet Servers => New => Server)
	* Saisir � BankService � pour le nom
	* Ajouter le projet � BankService �
	* Valider
	* Dans l�onglet � Server �, double cliquer sur le serveur � BankService �
	* Dans l�onglet port, modifier les diff�rents ports :
		* Tomcat admin port : 8007
		* HTTP/1.1 : 8095
		* AJP/1.3 : 8011
--------------- 

# Lancement du projet


* Lancer le Tomcat "RentCarsREST"
* Lancer le serveur "CarServer"  (CarServer.java)
* Lancer le Tomcat "BankService"
* Lancer le Tomcat "UPEMCarsService"




