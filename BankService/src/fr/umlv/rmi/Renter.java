package fr.umlv.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Renter extends Remote{
	
	
	
	public User authentication(String email, String password) throws RemoteException;
	public User findEmployeeByEmail(String email) throws RemoteException;
	
}
