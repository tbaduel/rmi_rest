package fr.umlv.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface User extends Remote{
	public String getName() throws RemoteException;
	public String getFirstName() throws RemoteException;
	public String getPassword() throws RemoteException;
	public String getEmail() throws RemoteException;
	public BankAccount getBankAccount() throws RemoteException;
	public boolean isEqualTo(User other) throws RemoteException;
}
