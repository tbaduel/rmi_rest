package fr.umlv.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Objects;

import fr.umlv.bankService.bank.Account;

public class BankAccountImpl extends UnicastRemoteObject implements BankAccount {
	

	private static final long serialVersionUID = 1L;
	public HashMap<String, Account> bankAccounts;
	
	public BankAccountImpl() throws RemoteException {
		super();
		bankAccounts = new HashMap<>();
	}

	public Account getAccountFromUser(String email) {
		if (email != null) {
			return bankAccounts.get(email);
		}
		return null;
	}
	
	public Account getAccountFromEmail(String email) {
		System.out.println("my email: " + email);
		Objects.requireNonNull(email);
		System.out.println(bankAccounts.get(email));
		return bankAccounts.get(email);
	}
	
	public void addUserAccount(String user, Account account) {
		System.out.println("PUTTING INTO BANKACCOUNTS: " + user);
		System.out.println(bankAccounts.put(user, account));
		System.out.println("ACCOUNT: " + account.getAccountValue());
	}


}
