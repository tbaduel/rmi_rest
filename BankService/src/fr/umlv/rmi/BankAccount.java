package fr.umlv.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import fr.umlv.bankService.bank.Account;

public interface BankAccount extends Remote{
	public Account getAccountFromUser(String email) throws RemoteException;
	public Account getAccountFromEmail(String email) throws RemoteException;
	public void addUserAccount(String user, Account account) throws RemoteException;
}
