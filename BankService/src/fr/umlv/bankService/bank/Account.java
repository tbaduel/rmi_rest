package fr.umlv.bankService.bank;

import java.io.Serializable;
import java.math.BigDecimal;

import fr.umlv.rmi.User;

public class Account implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private User user;
	private BigDecimal value;
	private String currency;
	
	
	public Account(User user, BigDecimal value, String currency) {
		this.user = user;
		this.value = value;
		this.currency = currency;
	}
	
	
	public BigDecimal getAccountValue() {
		return value;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void addValueToAccount(BigDecimal value) {
		value.add(value);
	}
	
	public User getUser() {
		return user;
	}
	
	public void setValue(BigDecimal val) {
		this.value = val;
	}
	
	public boolean transaction(BigDecimal val) {
		if (value.compareTo(val) == -1) {
			return false;
		}
		System.out.println("val: " + val.intValue() + " test1: " + value.intValue());
		this.value = value.subtract(val);
		System.out.println("test2: " + value.intValue());
		return true;
		
	}
}
