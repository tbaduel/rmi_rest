package fr.umlv.bankService.serverInit;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.umlv.rmi.BankAccount;
import fr.umlv.rmi.BankAccountImpl;


/**
 * Servlet implementation class ServerInit
 */
@WebServlet("/ServerInit")
public class ServerInit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * Default constructor. 
     */
    public ServerInit() {
        
    	
		try {
			// On recupere renter
			//LocateRegistry.createRegistry(1099);
			System.setProperty("java.security.policy","file:./security.policy");
			BankAccount bankAccount = new BankAccountImpl();
			Naming.rebind("BankAccount", bankAccount);
			
		} catch (MalformedURLException | RemoteException e) {
			System.out.println("ServerInit");
			e.printStackTrace();

		}
    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
