package fr.umlv.bankService.rest;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.umlv.bankService.bank.Account;
import fr.umlv.bankService.json.UserWalletCurrencyJSON;
import fr.umlv.rmi.BankAccount;
import fr.umlv.rmi.Renter;
import fr.umlv.rmi.User;

@Path("/bank")
public class BankServices {
	
	
	@Path("/getWallet")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAccountValue(@QueryParam("user")String email) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		BankAccount bank;
		try {
			bank = (BankAccount)Naming.lookup("BankAccount");
			System.out.println("getting wallet");
			
			Account account = bank.getAccountFromEmail(email);
			if (account != null) {
				BigDecimal value = account.getAccountValue();
				responseBuilder.add("value", value);
				responseBuilder.add("currency", account.getCurrency());
				
			}
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	
	@POST
	@Path("/addUserAccount")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addUserToBank(UserWalletCurrencyJSON userWalletCurrency) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		BankAccount bank;
		Renter renter;
		User user;
		boolean success = false;
		try {
			bank = (BankAccount)Naming.lookup("BankAccount");
			renter = (Renter) Naming.lookup("Renter");
			user = renter.findEmployeeByEmail(userWalletCurrency.getUserEmail());
			Account account = new Account(user,userWalletCurrency.getValue(),userWalletCurrency.getCurrency());
			System.out.println("ADDING USER INTO BANK: " + user.getEmail());
			bank.addUserAccount(user.getEmail(), account);
			System.out.println(bank.getAccountFromEmail(user.getEmail()).getAccountValue());
			success = true;
		} catch (RemoteException | MalformedURLException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		responseBuilder.add("success", success);
		
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	
	@POST
	@Path("/removeMoney")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String removeMoney(UserWalletCurrencyJSON uwc) {
		JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
		BankAccount bank;
		boolean success = false;
		try {
			bank = (BankAccount)Naming.lookup("BankAccount");
			System.out.println("Remove Money");
			
			Account account = bank.getAccountFromEmail(uwc.getUserEmail());
			if (account != null) {
				success = account.transaction(uwc.getValue());
				if (success) {
					account.setValue(account.getAccountValue().subtract(uwc.getValue()));
				}
				System.out.println("Bank transaction = " + success);
				
			}
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		responseBuilder.add("success", success);
		JsonObject responseJsonObject = responseBuilder.build();
		return responseJsonObject.toString();
	}
	//
}
