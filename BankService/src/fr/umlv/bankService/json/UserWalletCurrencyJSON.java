package fr.umlv.bankService.json;

import java.io.Serializable;
import java.math.BigDecimal;

public class UserWalletCurrencyJSON implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String userEmail;
	private BigDecimal value;
	private String currency;
	
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}
